/// https://github.com/tomaka/glium/issues/1461

// uses modified code from glium examples: https://github.com/tomaka/glium
// license: https://github.com/tomaka/glium/blob/master/LICENSE

#[macro_use]
extern crate glium;

use std::borrow::Cow;

fn main() {
    use glium::{DisplayBuild, Surface};
    let display = glium::glutin::WindowBuilder::new().with_dimensions(800, 600).build_glium().unwrap();

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
    }
    impl ::glium::vertex::Vertex for Vertex {
        #[inline]
        fn build_bindings() -> ::glium::vertex::VertexFormat {
            use std::borrow::Cow;
            Cow::Owned(<[_]>::into_vec(::std::boxed::Box::new(
                [
                    (Cow::Borrowed("position"),
                    {
                        let dummy:
                                &Vertex =
                            unsafe
                            {
                                ::std::mem::transmute(0usize)
                            };
                        let dummy_field =
                            &dummy.position;
                        let dummy_field:
                                usize =
                            unsafe
                            {
                                ::std::mem::transmute(dummy_field)
                            };
                        dummy_field
                    },
                    {
                        fn attr_type_of_val<T: ::glium::vertex::Attribute>(_:
                                                                               &T)
                         ->
                             ::glium::vertex::AttributeType {
                            <T as
                                ::glium::vertex::Attribute>::get_type()
                        }
                        let dummy:
                                &Vertex =
                            unsafe
                            {
                                ::std::mem::transmute(0usize)
                            };
                        attr_type_of_val(&dummy.position)
                    })
                ]
            )))
        }
    }

    let shape = {
        let vertex1 = Vertex { position: [-0.5, -0.5] };
        let vertex2 = Vertex { position: [ 0.0,  0.5] };
        let vertex3 = Vertex { position: [ 0.5, -0.25] };

        vec![vertex1, vertex2, vertex3]
    };
    /*let vertex_buffer1 = glium::VertexBuffer::new(&display, &shape).unwrap();*/
    let vertex_buffer1 = (unsafe {
        glium::VertexBuffer::new_raw(&display, &shape, <Vertex as glium::vertex::Vertex>::build_bindings(), 2 * ::std::mem::size_of::<f32>())
    }).unwrap();
    //*******************************************
    //still doesn't work, produces the same result as the "position" bindings:
    /*let bindings = Cow::Owned(vec![
        (
            Cow::Borrowed("x"), 0,
            glium::vertex::AttributeType::F32,
        ),
        (
            Cow::Borrowed("y"), ::std::mem::size_of::<f32>(),
            glium::vertex::AttributeType::F32,
        ),
    ]);*/
    let bindings = Cow::Owned(vec![(
            Cow::Borrowed("position"), 0,
            glium::vertex::AttributeType::F32F32,
        ),
    ]);
    let data = [
        -0.5_f32, -0.5_f32,
         0.0,  0.5,
         0.5, -0.25,
    ];
    /*let data = [
        [-0.5_f32, -0.5_f32],
        [ 0.0,  0.5],
        [ 0.5, -0.25],
    ];*/
    //even works for this:
    /*let data = [
        (-0.5_f32, -0.5_f32),
        ( 0.0,  0.5),
        ( 0.5, -0.25),
    ];*/

    println!("{}", data.len());
    println!("{}", ::std::mem::size_of::<f32>());
    println!("format1: {:?}", <Vertex as glium::vertex::Vertex>::build_bindings());
    println!("format2: {:?}", bindings);

    let vertex_buffer2 = (unsafe {
        glium::VertexBuffer::new_raw(&display, &data, bindings, 2 * ::std::mem::size_of::<f32>())
    }).unwrap();

    let indices = glium::index::NoIndices(glium::index::PrimitiveType::LineStrip);

    let program1 = {
        let vertex_shader_src = r#"
            #version 140

            in vec2 position;

            void main() {
                gl_Position = vec4(position, 0.0, 1.0);
            }
        "#;

        let fragment_shader_src = r#"
            #version 140

            out vec4 color;

            void main() {
                color = vec4(1.0, 0.0, 0.0, 0.5);
            }
        "#;

        glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None).unwrap()
    };

    let program2 = {
        let vertex_shader_src = r#"
            #version 140

            in vec2 position;
            //in float x;
            //in float y;

            void main() {
                gl_Position = vec4(position, 0.0, 1.0);
                //gl_Position = vec4(x, y, 0.0, 1.0);
            }
        "#;

        let fragment_shader_src = r#"
            #version 140

            out vec4 color;

            void main() {
                color = vec4(0.0, 1.0, 0.0, 0.5);
            }
        "#;

        glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None).unwrap()
    };

    let draw_parameters = glium::draw_parameters::DrawParameters {
        blend: glium::Blend::alpha_blending(),
        .. Default::default()
    };

    loop {
        let mut target = display.draw();
        target.clear_color(0.0, 0.0, 1.0, 1.0);
        target.draw(&vertex_buffer1, &indices, &program1, &glium::uniforms::EmptyUniforms,
                    &draw_parameters).unwrap();
        target.draw(&vertex_buffer2, &indices, &program2, &glium::uniforms::EmptyUniforms,
                    &draw_parameters).unwrap();
        target.finish().unwrap();

        for ev in display.poll_events() {
            match ev {
                glium::glutin::Event::Closed => return,
                _ => ()
            }
        }
    }
}
